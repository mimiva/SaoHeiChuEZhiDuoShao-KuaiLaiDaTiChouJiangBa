// 本地数据库
class IndexDB {
    public static IDB = null;
    public static DB = null;
    // 设置本地数据库
    static SET_INDEXED_DB_FUN() {
        const indexedDB = window.indexedDB;
        const log = console.log;

        if (!indexedDB) {
            alert('你的设备设置不支持IndexedDB');
            window.location.href = `${ParamsPool.PROJECT_RUL}?${Math.random()}`;
            return;
        }

        IndexDB.IDB = indexedDB.open(`${ParamsPool.GAME_NAME}_${ParamsPool.GAME_VERSION}`, 5);

        IndexDB.IDB.onsuccess = e => {
            log('本地数据库连接成功!');
            IndexDB.DB = e.target.result;
            log(IndexDB.DB)
            IndexDB.GET_GAME_NUM_FUN_IDB();
        };

        IndexDB.IDB.onerror = e => log(e.currentTarget.error.message);

        IndexDB.IDB.onupgradeneeded = e => {
            log('数据库升级事件')
            IndexDB.DB = e.target.result;
            let objStore = null;
            if (!IndexDB.DB.objectStoreNames.contains('game_num')) {
                objStore = IndexDB.DB.createObjectStore('game_num', { keyPath: 'miva_id' });
            }
            log(objStore)
        }
    }
    // indexedDB初始化游戏次数方法
    static ADD_GAME_NUM_FUN_IDB() {
        const log = console.log;
        const req = IndexDB.DB.transaction(['game_num'], 'readwrite')
            .objectStore('game_num')
            .add({ miva_id: 1, day: new Date().getDate(), num: ParamsPool.GAME_NUM });

        req.onsuccess = evt => {
            log('游戏次数设置成功, 剩余:', ParamsPool.GAME_NUM, '次');
        }

        req.onerror = evt => {
            log('游戏次数设置失败');
            alert('游戏次数设置失败(初始化)');
            window.location.href = `${ParamsPool.PROJECT_RUL}?${Math.random()}`;
        }
    }
    // indexedDB设置游戏次数方法
    static SET_GAME_NUM_FUN_IDB() {
        const log = console.log;
        const req = IndexDB.DB.transaction(['game_num'], 'readwrite')
            .objectStore('game_num')
            .put({ miva_id: 1, day: new Date().getDate(), num: ParamsPool.GAME_NUM });

        req.onsuccess = evt => {
            log('游戏次数设置成功, 剩余:', ParamsPool.GAME_NUM, '次');
        }

        req.onerror = evt => {
            log('游戏次数设置失败');
            alert('游戏次数设置失败(更新)');
            window.location.href = `${ParamsPool.PROJECT_RUL}?${Math.random()}`;
        }
    }
    // 从indexedDb中读取游戏次数
    static GET_GAME_NUM_FUN_IDB() {
        const log = console.log;
        const transaction = IndexDB.DB.transaction(['game_num']);
        const objectStore = transaction.objectStore('game_num');
        const req = objectStore.get(1);

        req.onerror = evt => {
            log('事务失败');
            window.location.href = `${ParamsPool.PROJECT_RUL}?${Math.random()}`;
        }

        req.onsuccess = evt => {
            if (req.result) {
                log(req.result);
                if (req.result.day != new Date().getDate()) return IndexDB.SET_GAME_NUM_FUN_IDB();
                ParamsPool.GAME_NUM = req.result.num;
                // Start.txt.text = ` 今日剩余 ${ParamsPool.GAME_NUM} 次`;
                log(` 今日剩余 ${ParamsPool.GAME_NUM} 次`)
            } else {
                log('未获得数据记录');
                IndexDB.ADD_GAME_NUM_FUN_IDB();
            }
        }
    }
}