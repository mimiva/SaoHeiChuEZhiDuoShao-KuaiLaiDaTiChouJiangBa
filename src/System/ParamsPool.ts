// 变量池
class ParamsPool {
    // 问题集
    static QUESTION_BOX = [];
    // 题号
    static QUESTION_NUM = 1;
    // 答对数
    static QUESTION_SUCCESS = 0;
    // 回答多少道题时结束
    static OVER_NUM = 10;
    // 正确多少道时可以抽奖
    static ROLL_NUM = 9;
    // 游戏次数
    static GAME_NUM = 1;
    // 游戏名
    static GAME_NAME = 'gaolin';
    // 游戏版本号
    static GAME_VERSION = '0.0.6';
    // 数据接口地址
    static BASE_RUL = 'http://59.110.214.170:23333';
    // 项目地址
    static PROJECT_RUL = '';
    // 用户信息
    static USER_INFO = {
        name: null,
        occ: null,
        phone: null,
    };
    // BGM
    static BGM = null;
    static SOUND_CHANNEL = null;

}