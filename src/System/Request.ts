// 封装egret的网络请求
class Request_miva {
    public static async POST(link: string, method: string, data) {
        const request = new egret.HttpRequest();
        const HttpMethod = method == 'post' || method == 'POST'
            ? egret.HttpMethod.POST
            : egret.HttpMethod.GET;
        request.responseType = egret.HttpResponseType.TEXT;
        request.open(ParamsPool.BASE_RUL + link, HttpMethod);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.send(data);
        request.addEventListener(egret.Event.COMPLETE, evt => {
            console.log(evt.currentTarget.response)
            const res = JSON.parse(evt.currentTarget.response);
            // if (res.length == 0) return;
            // ParamPool.BEST_SCORE = res[0].score;
        }, this);
    }
}