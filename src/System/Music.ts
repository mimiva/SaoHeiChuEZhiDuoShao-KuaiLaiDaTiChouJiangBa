class MusicItem extends egret.DisplayObjectContainer {

    private soundBtn: egret.Bitmap;
    private isPlay;

    public constructor() {
        super();
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this);
    }

    private init() {
        this.startLoad();
    }

    private startLoad(): void {
        this.onLoadComplete();
    }

    private onLoadComplete(): void {
        //一个简单的播放按钮
        this.soundBtn = Utils.createBitmapByName("bgm_open_png");
        //监听按钮的触摸事件
        this.soundBtn.touchEnabled = true;
        this.soundBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTouch, this);
        this.addChild(this.soundBtn);
    }

    private onTouch() {

        if (ParamsPool.SOUND_CHANNEL) {
            this.soundBtn.texture = RES.getRes("bgm_close_png");
            console.log(ParamsPool.SOUND_CHANNEL);
            ParamsPool.SOUND_CHANNEL.stop();
            ParamsPool.SOUND_CHANNEL = null;
            return;
        } else {
            this.soundBtn.texture = RES.getRes("bgm_open_png");
        }

        ParamsPool.SOUND_CHANNEL = ParamsPool.BGM.play(0, -1);
    }

}