// 获取用户信息
class GetUserInfo extends egret.DisplayObjectContainer {
    private phoneInput = new eui.TextInput();
    private nameInput = new eui.TextInput();
    private occInput = new eui.TextInput();

    public constructor() {
        super();
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this);
    }

    private init() {
        const miva = this;
        const form_png = Utils.createBitmapByName('form_png');
        miva.addChild(form_png);
        miva.createForm();
        miva.createBtn();
    }
    // 创建表单
    private createForm() {
        const miva = this;
        const container = new egret.Sprite();
        const arr = [
            { type: "name", target: miva.nameInput },
            { type: "occ", target: miva.occInput },
            { type: "phone", target: miva.phoneInput }
        ];
        for (const temp in arr) {
            const target = arr[temp].target;
            const type = arr[temp].type;
            target.y = 90 * parseInt(temp);
            container.addChild(target);
            ParamsPool.USER_INFO[type] = null;
            target.addEventListener(egret.Event.CHANGE, () => {
                ParamsPool.USER_INFO[type] = target.text;
            }, miva);
        }
        container.x = 210;
        container.y = 500;
        miva.addChild(container);
    }
    // 创建按钮
    private createBtn() {
        const miva = this;
        const btn = Utils.createBitmapByName('btn_done_png');
        miva.addChild(btn);
        Utils.dragGetPatn(btn, miva, {
            startFun: handler
        }, false);
        btn.x = 195;
        btn.y = 825;
        function handler() {
            console.log('用户信息', ParamsPool.USER_INFO);
            for (const temp in ParamsPool.USER_INFO)
                if (!ParamsPool.USER_INFO[temp])
                    return console.log('用户信息不完整');
            console.log('去抽奖页面')
            miva.dispatchEvent(new egret.Event("start"));
        }
    }
}