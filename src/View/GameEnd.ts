
class GameEnd extends egret.DisplayObjectContainer {
    public constructor() {
        super();
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this);
    }

    private init() {
        const miva = this;
        const end_png = ParamsPool.QUESTION_SUCCESS < ParamsPool.ROLL_NUM
            ? Utils.createBitmapByName('roll_error_png')
            : Utils.createBitmapByName('roll_success_png');
        miva.addChild(end_png);
        Utils.createText(`${ParamsPool.QUESTION_NUM - 1}题`, { x: 65, y: 515 }, miva, false);
        Utils.createText(`${ParamsPool.QUESTION_SUCCESS}题`, { x: 275, y: 515 }, miva, false);
        Utils.createText(`${ParamsPool.QUESTION_NUM - 1 - ParamsPool.QUESTION_SUCCESS}题`, { x: 475, y: 515 }, miva, false);
        const name = Utils.createText(`${ParamsPool.USER_INFO.name}`, { x: 0, y: 300 }, miva, false);
        name.textColor = 0;
        name.stroke = 0;
        name.width = miva.stage.stageWidth;
        name.textAlign = egret.HorizontalAlign.CENTER;
        miva.createBtn();
    }
    // 创建按钮
    private createBtn() {
        const miva = this;
        const btn = new egret.Sprite();
        btn.graphics.beginFill(0, 0);
        btn.graphics.drawRect(0, 0, 260, 70);
        btn.graphics.endFill();
        btn.x = miva.stage.stageWidth / 2;
        btn.y = 850;
        miva.addChild(btn);
        Utils.anchorCenter(btn);
        Utils.dragGetPatn(btn, miva, {
            startFun: handler
        }, false);
        function handler() {
            const event = ParamsPool.QUESTION_SUCCESS < ParamsPool.ROLL_NUM
                ? new egret.Event('replay')
                : new egret.Event('roll');
            miva.dispatchEvent(event);
        }
    }
}