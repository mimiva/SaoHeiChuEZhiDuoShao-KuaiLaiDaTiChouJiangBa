// 开始场景
class StartScreen extends egret.DisplayObjectContainer {
    private start_bg_png: egret.Bitmap = Utils.createBitmapByName('home_png')
    private start_btn;
    private isMusic = true;
    public constructor() {
        super()
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this)
    }

    private init() {
        const miva = this;
        miva.addChild(miva.start_bg_png);
        miva.createStartBtn();
        miva.createRuleBtn();

        const text = Utils.createText(`今日剩余 ${ParamsPool.GAME_NUM} 次`, { x: 215, y: 650 }, miva, false);
        miva.addEventListener(egret.Event.ENTER_FRAME, () => {
            text.text = `今日剩余 ${ParamsPool.GAME_NUM} 次`
            text.size = 30;
        }, miva);

        miva.createAlert();
        ParamsPool.BGM = RES.getRes("mp3_mp3");
    }
    // 创建次数不足提示
    private createNumTipAlert() {
        const miva = this;
        const tk_png = Utils.createBitmapByName("tk_png");
        miva.addChild(tk_png);
        Utils.anchorCenter(tk_png);
        tk_png.x = miva.stage.stageWidth / 2;
        tk_png.y = miva.stage.stageHeight / 2;
        egret.Tween.get(tk_png).wait(500).to({ alpha: 0 }, 200);
        console.log("游戏次数不足 !!")
    }
    // 创建开始按钮
    private createStartBtn() {
        const miva = this;
        const start_btn = miva.start_btn = this.createBtn();
        start_btn.x = 185;
        start_btn.y = 845;
        this.addChild(start_btn);
        Utils.dragGetPatn(start_btn, this, {
            startFun: touchStartHandler
        }, false);
        function touchStartHandler() {
            if (ParamsPool.GAME_NUM <= 0) {
                miva.createNumTipAlert()
                return console.log("游戏次数不足!!")
            }
            console.log("游戏开始 !!");
            ParamsPool.GAME_NUM--;
            IndexDB.SET_GAME_NUM_FUN_IDB();
            miva.dispatchEvent(new egret.Event("start"));
        }
    }
    // 创建规则按钮
    private createRuleBtn() {
        const miva = this;
        const rule_btn = this.createBtn();
        rule_btn.x = 185;
        rule_btn.y = 705;
        this.addChild(rule_btn);
        Utils.dragGetPatn(rule_btn, this, {
            startFun: touchStartHandler
        }, false);

        function touchStartHandler(evt) {
            console.log(`查看规则`);
            miva.createAlert();
        }
    }
    // 生成规则弹窗 可抽象为一个新工具
    private createAlert() {
        const miva = this
        const container = new egret.Sprite();
        miva.stage.addChild(container);
        Utils.createMask(container, handler);
        const alert_bg = Utils.createBitmapByName("rule_png");
        container.addChild(alert_bg);
        Utils.anchorCenter(alert_bg);
        alert_bg.x = miva.stage.stageWidth / 2;
        alert_bg.y = miva.stage.stageHeight / 2;
        alert_bg.touchEnabled = true;
        alert_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, Utils.superStopPropagation, miva);
        const close_btn = Utils.createBitmapByName("close_png");
        close_btn.x = 490;
        close_btn.y = 200;
        container.addChild(close_btn);
        close_btn.touchEnabled = true;
        close_btn.addEventListener(egret.TouchEvent.TOUCH_TAP, handler, miva);

        function handler(evt) {
            if (miva.isMusic) {
                ParamsPool.SOUND_CHANNEL = ParamsPool.BGM.play(0, -1);
                miva.isMusic = false;
            }
            miva.stage.removeChild(container);
        }
    }
    // 生成按钮方法
    private createBtn() {
        const btn = new egret.Sprite();
        btn.graphics.beginFill(0, 0);
        btn.graphics.drawRect(0, 0, 260, 70);
        btn.graphics.endFill();
        btn.touchEnabled = true;
        return btn;
    }
}