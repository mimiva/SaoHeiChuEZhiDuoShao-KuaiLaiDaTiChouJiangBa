// 场景主控
class ScreenLayer extends egret.DisplayObjectContainer {
    private startScreen: StartScreen = new StartScreen();

    public constructor() {
        super()
        this.once(egret.Event.ADDED_TO_STAGE, this.init, this)
    }

    private init() {
        const miva = this;
        // 初始化游戏配置
        miva.resetInit();
        // 设置本地数据库
        // IndexDB.SET_INDEXED_DB_FUN();
        let canFenxiang = false;
        setTimeout(() => {
            // 在这里执行一个查询事务
            const transaction = IndexDB.DB.transaction(['game_num']);
            const objectStore = transaction.objectStore('game_num');
            const req = objectStore.get(2);
            req.onsuccess = evt => {
                // 如果查成功了 处理结果
                if (req.result) {
                    console.log('查到了')
                    // 如果查到了并且时间不是今天, 那就让今天可以获得次数
                    if (req.result.day != new Date().getDate()) {
                        console.log('时间不是今天')
                        const req = IndexDB.DB.transaction(['game_num'], 'readwrite')
                            .objectStore('game_num')
                            .put({ miva_id: 2, day: new Date().getDate(), is: true });
                        canFenxiang = true;
                        // 然后启动定时器, 判断是否分享了
                        pengyou();
                        return;
                    }
                    // 如果查到了, 但是今天可以获得次数就启动定时器, 判断是否分享了
                    if (req.result.is) {
                        console.log('可以分享')
                        canFenxiang = true;
                        pengyou();
                        return;
                    }
                    canFenxiang = false;
                } else {
                    // 如果没有查到就增加一条
                    console.log('没有查到, 增加了可分享的次数')
                    const req = IndexDB.DB.transaction(['game_num'], 'readwrite')
                        .objectStore('game_num')
                        .add({ miva_id: 2, day: new Date().getDate(), is: true });
                    canFenxiang = true;
                    // 然后启动定时器, 判断是否分享了
                    pengyou();
                }
            }
        }, 2000)

        function pengyou() {
            setInterval(() => {
                try {
                    const miva = document.querySelector("#miva");
                    if (miva.innerHTML == 'mimiva' && canFenxiang) {
                        ParamsPool.GAME_NUM++;
                        IndexDB.SET_GAME_NUM_FUN_IDB();
                        miva.innerHTML = '';
                        console.log('分享了, 消耗掉分享次数')
                        IndexDB.DB.transaction(['game_num'], 'readwrite')
                            .objectStore('game_num')
                            .put({ miva_id: 2, day: new Date().getDate(), is: false });
                        canFenxiang = false;
                    }
                } catch (err) { }
            }, 500);
        }

    }
    // 初始化
    private resetInit() {
        const miva = this;
        const data = RES.getRes('ParamsPool_json');
        ParamsPool.QUESTION_BOX = RES.getRes("question_json");
        (<any>Object).assign(ParamsPool, data);
        // 题号
        ParamsPool.QUESTION_NUM = 1;
        // 答对数
        ParamsPool.QUESTION_SUCCESS = 0;

        ParamsPool.USER_INFO = {
            name: null,
            occ: null,
            phone: null,
        };
        // 设置本地数据库
        IndexDB.SET_INDEXED_DB_FUN();
        miva.addChild(miva.startScreen);
        miva.startScreen.addEventListener("start", miva.startGameHandler, this);
    }
    // 游戏开始处理器
    private startGameHandler() {
        const miva = this;
        miva.removeChild(miva.startScreen);
        const getUserInfo = new GetUserInfo();
        // const getUserInfo = new GameRoll();
        miva.addChild(getUserInfo)
        getUserInfo.addEventListener("start", () => {
            miva.removeChild(getUserInfo);
            miva.runGameHandler()
        }, this);
    }
    // 游戏运行中处理器
    private runGameHandler() {
        const miva = this;
        const gameStation: GameStation = new GameStation();
        miva.addChild(gameStation);
        gameStation.addEventListener("next", () => {
            miva.removeChild(gameStation);
            miva.runGameHandler();
        }, this);
        gameStation.addEventListener("over", miva.gameOverHandler(gameStation), this);
    }
    // 游戏结束处理器
    private gameOverHandler(gameStation) {
        return () => {
            const miva = this;
            console.log("答题结束")
            miva.removeChild(gameStation);
            const gameEnd = new GameEnd();
            miva.addChild(gameEnd);
            gameEnd.addEventListener('replay', miva.resetHandler(gameEnd), miva);
            gameEnd.addEventListener('roll', miva.gotoGameRollhandler(gameEnd), miva);
        }
    }
    // 进入抽奖场景处理器
    private gotoGameRollhandler(gameEnd) {
        return () => {
            const miva = this;
            miva.removeChild(gameEnd);
            const gameRoll = new GameRoll();
            miva.addChild(gameRoll);
            gameRoll.addEventListener('replay', miva.resetHandler(gameRoll), miva);
        }
    }
    // 重置游戏处理器
    private resetHandler(item) {
        return () => {
            const miva = this;
            miva.removeChild(item);
            miva.resetInit();
        }
    }

}