<?php
require_once "jssdk.php";
$jssdk = new jssdk("wxe6de21287cb9e5eb", "7b550f9b1bd51a23146468c675d3a61f");
$signPackage = $jssdk->getSignPackage();
?>
<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="full-screen" content="true" />
    <meta name="screen-orientation" content="portrait" />
    <meta name="x5-fullscreen" content="true" />
    <meta name="360-fullscreen" content="true" />
    <style>
        html, body {
            -ms-touch-action: none;
            background: #000;
            padding: 0;
            border: 0;
            margin: 0;
            height: 100%;
        }
    </style>
<script src="./jquery.js"></script>
<script src="./jweixin.js"></script>
<script type="text/javascript">
        wx.config({
            debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
            appId: "<?php echo $signPackage['appId']; ?>", // 必填，公众号的唯一标识
            timestamp: "<?php echo $signPackage['timestamp']; ?>", // 必填，生成签名的时间戳
            nonceStr: "<?php echo $signPackage['nonceStr']; ?>",   // 必填，生成签名的随机串
            signature: "<?php echo $signPackage['signature']; ?>", // 必填，签名，见附录1
            jsApiList: [
                'onMenuShareTimeline',
                'onMenuShareAppMessage'
              ] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
        });
            
        wx.ready(function(){
            var share = {
                title:'扫黑除恶知多少？快来答题抽奖吧！', 
                desc: '高陵区卫健局扫黑除恶专项斗争有奖问答活动',  // 分享描述
                link: 'http://www.unmcc.com/unmcc/2019/03/gaolin/', // 分享链接
                imgUrl: 'http://www.unmcc.com/unmcc/2019/03/gaolin/fenxiang.jpg', // 分享图标
                type: 'link', // 分享类型,music、video或link，不填默认为link
                dataUrl: '',  // 如果type是music或video，则要提供数据链接，默认为空
                success: function () { 
                    // 用户确认分享后执行的回调函数
                    $.post("{:U('Index/setshare')}", function(data){
                        if(data.status == 1){
                            window.location.href = "{:U('Index/index')}";
                        }
                        return false;
                    });

                    $('#miva').html('mimiva')
                },
                cancel: function () { 
                    // 用户取消分享后执行的回调函数
                }
            }
        
            //“分享到朋友圈”按钮点击状态及自定义分享内容接口
            wx.onMenuShareTimeline(share);
            
            //“分享给朋友”按钮点击状态及自定义分享内容接口
            wx.onMenuShareAppMessage(share);
            
        });

        wx.error(function(res){
           // alert(res.errMsg);
        });
</script>
</head>

<body>

    <div style="display: none" id="miva"></div>

    <div style="margin: auto;width: 100%;height: 100%;" class="egret-player"
         data-entry-class="Main"
         data-orientation="portrait"
         data-scale-mode="showAll"
         data-frame-rate="60"
         data-content-width="640"
         data-content-height="1136"
         data-show-paint-rect="false"
         data-multi-fingered="2"
         data-show-fps="false" data-show-log="false"
         data-show-fps-style="x:0,y:0,size:12,textColor:0xffffff,bgAlpha:0.9">
    </div>
<script>
    var loadScript = function (list, callback) {
        var loaded = 0;
        var loadNext = function () {
            loadSingleScript(list[loaded], function () {
                loaded++;
                if (loaded >= list.length) {
                    callback();
                }
                else {
                    loadNext();
                }
            })
        };
        loadNext();
    };

    var loadSingleScript = function (src, callback) {
        var s = document.createElement('script');
        s.async = false;
        s.src = src;
        s.addEventListener('load', function () {
            s.parentNode.removeChild(s);
            s.removeEventListener('load', arguments.callee, false);
            callback();
        }, false);
        document.body.appendChild(s);
    };

    var xhr = new XMLHttpRequest();
    xhr.open('GET', './manifest.json?v=' + Math.random(), true);
    xhr.addEventListener("load", function () {
        var manifest = JSON.parse(xhr.response);
        var list = manifest.initial.concat(manifest.game);
        loadScript(list, function () {
            /**
             * {
             * "renderMode":, //Engine rendering mode, "canvas" or "webgl"
             * "audioType": 0 //Use the audio type, 0: default, 2: web audio, 3: audio
             * "antialias": //Whether the anti-aliasing is enabled in WebGL mode, true: on, false: off, defaults to false
             * "calculateCanvasScaleFactor": //a function return canvas scale factor
             * }
             **/
            egret.runEgret({ renderMode: "webgl", audioType: 0, calculateCanvasScaleFactor:function(context) {
                var backingStore = context.backingStorePixelRatio ||
                    context.webkitBackingStorePixelRatio ||
                    context.mozBackingStorePixelRatio ||
                    context.msBackingStorePixelRatio ||
                    context.oBackingStorePixelRatio ||
                    context.backingStorePixelRatio || 1;
                return (window.devicePixelRatio || 1) / backingStore;
            }});
        });
    });
    xhr.send(null);
</script>
</body>

</html>