var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
// 获取用户信息
var GetUserInfo = (function (_super) {
    __extends(GetUserInfo, _super);
    function GetUserInfo() {
        var _this = _super.call(this) || this;
        _this.phoneInput = new eui.TextInput();
        _this.nameInput = new eui.TextInput();
        _this.occInput = new eui.TextInput();
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    GetUserInfo.prototype.init = function () {
        var miva = this;
        var form_png = Utils.createBitmapByName('form_png');
        miva.addChild(form_png);
        miva.createForm();
        miva.createBtn();
    };
    // 创建表单
    GetUserInfo.prototype.createForm = function () {
        var miva = this;
        var container = new egret.Sprite();
        var arr = [
            { type: "name", target: miva.nameInput },
            { type: "occ", target: miva.occInput },
            { type: "phone", target: miva.phoneInput }
        ];
        var _loop_1 = function (temp) {
            var target = arr[temp].target;
            var type = arr[temp].type;
            target.y = 90 * parseInt(temp);
            container.addChild(target);
            ParamsPool.USER_INFO[type] = null;
            target.addEventListener(egret.Event.CHANGE, function () {
                ParamsPool.USER_INFO[type] = target.text;
            }, miva);
        };
        for (var temp in arr) {
            _loop_1(temp);
        }
        container.x = 210;
        container.y = 500;
        miva.addChild(container);
    };
    // 创建按钮
    GetUserInfo.prototype.createBtn = function () {
        var miva = this;
        var btn = Utils.createBitmapByName('btn_done_png');
        miva.addChild(btn);
        Utils.dragGetPatn(btn, miva, {
            startFun: handler
        }, false);
        btn.x = 195;
        btn.y = 825;
        function handler() {
            console.log('用户信息', ParamsPool.USER_INFO);
            for (var temp in ParamsPool.USER_INFO)
                if (!ParamsPool.USER_INFO[temp])
                    return console.log('用户信息不完整');
            console.log('去抽奖页面');
            miva.dispatchEvent(new egret.Event("start"));
        }
    };
    return GetUserInfo;
}(egret.DisplayObjectContainer));
__reflect(GetUserInfo.prototype, "GetUserInfo");
