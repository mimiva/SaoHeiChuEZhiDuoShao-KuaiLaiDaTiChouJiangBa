var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var GameEnd = (function (_super) {
    __extends(GameEnd, _super);
    function GameEnd() {
        var _this = _super.call(this) || this;
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    GameEnd.prototype.init = function () {
        var miva = this;
        var end_png = ParamsPool.QUESTION_SUCCESS < ParamsPool.ROLL_NUM
            ? Utils.createBitmapByName('roll_error_png')
            : Utils.createBitmapByName('roll_success_png');
        miva.addChild(end_png);
        Utils.createText(ParamsPool.QUESTION_NUM - 1 + "\u9898", { x: 65, y: 515 }, miva, false);
        Utils.createText(ParamsPool.QUESTION_SUCCESS + "\u9898", { x: 275, y: 515 }, miva, false);
        Utils.createText(ParamsPool.QUESTION_NUM - 1 - ParamsPool.QUESTION_SUCCESS + "\u9898", { x: 475, y: 515 }, miva, false);
        var name = Utils.createText("" + ParamsPool.USER_INFO.name, { x: 0, y: 300 }, miva, false);
        name.textColor = 0;
        name.stroke = 0;
        name.width = miva.stage.stageWidth;
        name.textAlign = egret.HorizontalAlign.CENTER;
        miva.createBtn();
    };
    // 创建按钮
    GameEnd.prototype.createBtn = function () {
        var miva = this;
        var btn = new egret.Sprite();
        btn.graphics.beginFill(0, 0);
        btn.graphics.drawRect(0, 0, 260, 70);
        btn.graphics.endFill();
        btn.x = miva.stage.stageWidth / 2;
        btn.y = 850;
        miva.addChild(btn);
        Utils.anchorCenter(btn);
        Utils.dragGetPatn(btn, miva, {
            startFun: handler
        }, false);
        function handler() {
            var event = ParamsPool.QUESTION_SUCCESS < ParamsPool.ROLL_NUM
                ? new egret.Event('replay')
                : new egret.Event('roll');
            miva.dispatchEvent(event);
        }
    };
    return GameEnd;
}(egret.DisplayObjectContainer));
__reflect(GameEnd.prototype, "GameEnd");
