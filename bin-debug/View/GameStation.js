var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
// 主体内容
var GameStation = (function (_super) {
    __extends(GameStation, _super);
    function GameStation() {
        var _this = _super.call(this) || this;
        _this.game_bg = Utils.createBitmapByName('game_png');
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    GameStation.prototype.init = function () {
        var miva = this;
        miva.addChild(miva.game_bg);
        miva.nowQuestion = miva.getQuestion();
        miva.createQuestion();
        miva.createOptions();
        miva.createTotalNum();
    };
    // 抽取题目
    GameStation.prototype.getQuestion = function () {
        var num = Math.floor(Math.random() * (ParamsPool.QUESTION_BOX.length - 1));
        var res = ParamsPool.QUESTION_BOX[num];
        ParamsPool.QUESTION_BOX.splice(num, 1);
        console.log('当前题目是:', res);
        return res;
    };
    // 创建标题
    GameStation.prototype.createQuestion = function () {
        var miva = this;
        var question = new eui.Label();
        question.text = miva.nowQuestion.Q;
        question.width = 520;
        question.textColor = 0x2D3C52;
        question.x = 45;
        question.y = 150;
        question.lineSpacing = 15;
        question.bold = true;
        miva.addChild(question);
    };
    // 创建选项
    GameStation.prototype.createOptions = function () {
        var miva = this;
        var options = miva.nowQuestion.A;
        var num = 0;
        var line = 150;
        for (var temp in options) {
            if (!options[temp])
                return;
            createItem(options[temp], temp);
            num++;
        }
        function createItem(optionData, temp) {
            var option = new eui.Label();
            option.text = optionData;
            option.textAlign = egret.HorizontalAlign.CENTER;
            option.textColor = 0x2D3C52;
            option.bold = true;
            option.width = 520;
            option.size = 24;
            option.x = 60;
            option.y = 585 + (num * line);
            miva.addChild(option);
            Utils.dragGetPatn(option, miva, {
                startFun: touchHandler
            }, false);
            function touchHandler() {
                console.log("\u9009\u62E9\u4E86: %s%s", temp, optionData);
                temp == miva.nowQuestion.T + "."
                    ? miva.createAlert(true, { A: miva.nowQuestion.A[miva.nowQuestion.T + "."] })
                    : miva.createAlert(false, { A: miva.nowQuestion.A[miva.nowQuestion.T + "."] });
            }
        }
    };
    // 创建弹框
    GameStation.prototype.createAlert = function (result, data) {
        var miva = this;
        var container = new egret.Sprite();
        miva.addChild(container);
        Utils.createMask(container, closeHandler);
        if (result) {
            ParamsPool.QUESTION_SUCCESS++;
        }
        ParamsPool.QUESTION_NUM++;
        var alert_bg = result
            ? Utils.createBitmapByName("success_png")
            : Utils.createBitmapByName("error_png");
        container.addChild(alert_bg);
        Utils.anchorCenter(alert_bg);
        alert_bg.x = miva.stage.stageWidth / 2;
        alert_bg.y = miva.stage.stageHeight / 2;
        var closeBtn = ParamsPool.QUESTION_NUM <= 10
            ? Utils.createBitmapByName("btn_next_png")
            : Utils.createBitmapByName("btn_done_png");
        container.addChild(closeBtn);
        Utils.dragGetPatn(closeBtn, container, {
            startFun: closeHandler
        }, false);
        closeBtn.x = 180;
        closeBtn.y = 810;
        // 创建正确答案的选项字母
        miva.createT_alert(container);
        // 创建正确答案的全文
        miva.cteateT_text(container, data.A);
        function closeHandler() {
            miva.removeChild(container);
            ParamsPool.QUESTION_NUM <= 10
                ? miva.dispatchEvent(new egret.Event('next'))
                : miva.dispatchEvent(new egret.Event('over'));
        }
    };
    // 创建正确答案的选项字母
    GameStation.prototype.createT_alert = function (container) {
        var miva = this;
        var box = new egret.Sprite();
        var title = new eui.Label();
        title.text = '正确答案: ';
        title.textColor = 0x2D3C52;
        var txt = new eui.Label();
        txt.textColor = 0xf45050;
        txt.text = miva.nowQuestion.T;
        txt.x = title.width + 60;
        title.size = txt.size = 45;
        title.bold = txt.bold = true;
        box.x = 80;
        box.y = 460;
        box.addChild(title);
        box.addChild(txt);
        container.addChild(box);
    };
    // 创建正确答案的全文
    GameStation.prototype.cteateT_text = function (container, dataA) {
        var miva = this;
        var parser = new egret.HtmlTextParser();
        var textfield = new egret.TextField();
        // const cut = "( 请选择 )";
        var cut = "(___)";
        var replaceTxt = "<font color=0xf45050>" + dataA + "</font>";
        var textFlow = miva.nowQuestion.Q.replace(cut, replaceTxt);
        textfield.textFlow = parser.parse(textFlow);
        textfield.textColor = 0x2D3C52;
        textfield.lineSpacing = 15;
        textfield.bold = true;
        textfield.width = 480;
        textfield.anchorOffsetX = 240;
        textfield.x = miva.stage.stageWidth / 2;
        textfield.y = 525;
        textfield.size = 28;
        container.addChild(textfield);
    };
    // 创建顶部答题总数
    GameStation.prototype.createTotalNum = function () {
        var miva = this;
        var textfield = new egret.TextField();
        textfield.text = ParamsPool.QUESTION_NUM + "/" + ParamsPool.OVER_NUM;
        textfield.textColor = 0xffffff;
        textfield.lineSpacing = 15;
        textfield.bold = true;
        textfield.textAlign = egret.HorizontalAlign.CENTER;
        textfield.width = 480;
        textfield.anchorOffsetX = 240;
        textfield.x = miva.stage.stageWidth / 2;
        textfield.y = 10;
        textfield.size = 28;
        miva.addChild(textfield);
        Utils.dragGetPatn(textfield, miva);
    };
    return GameStation;
}(egret.DisplayObjectContainer));
__reflect(GameStation.prototype, "GameStation");
