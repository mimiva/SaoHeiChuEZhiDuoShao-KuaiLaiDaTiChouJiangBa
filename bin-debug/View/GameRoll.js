var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
// 抽奖页面
var GameRoll = (function (_super) {
    __extends(GameRoll, _super);
    function GameRoll() {
        var _this = _super.call(this) || this;
        _this.item_pan_png = Utils.createBitmapByName('roll_pan_png');
        _this.res_png = new egret.Bitmap();
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    GameRoll.prototype.init = function () {
        var miva = this;
        var bg = Utils.createBitmapByName('roll_bg_png');
        miva.addChild(bg);
        miva.createRollItem();
        miva.createTopTip('roll_default_png');
        var name = Utils.createText("" + ParamsPool.USER_INFO.name, { x: 0, y: 140 }, miva, false);
        name.textColor = 0;
        name.stroke = 0;
        name.width = miva.stage.stageWidth;
        name.textAlign = egret.HorizontalAlign.CENTER;
    };
    // 创建顶部文字
    GameRoll.prototype.createTopTip = function (png_name) {
        var miva = this;
        miva.res_png.texture = RES.getRes(png_name);
        miva.addChild(miva.res_png);
        Utils.anchorCenter(miva.res_png);
        // Utils.dragGetPatn(miva.res_png, miva);
        miva.res_png.x = miva.stage.stageWidth / 2;
        miva.res_png.y = 320;
    };
    // 创建转盘
    GameRoll.prototype.createRollItem = function () {
        var miva = this;
        var item_pan_png = miva.item_pan_png;
        var item_zhen_png = Utils.createBitmapByName("roll_zhen_png");
        var roll_box = [
            { dag: 0, text: '三等奖', res: 3 },
            { dag: 60, text: '谢谢参与', res: 0 },
            { dag: 120, text: '一等奖', res: 1 },
            { dag: 180, text: '三等奖', res: 3 },
            { dag: 240, text: '谢谢参与', res: 0 },
            { dag: 300, text: '二等奖', res: 2 }
        ];
        item_pan_png.x = miva.stage.stageWidth / 2;
        item_pan_png.y = 690;
        item_zhen_png.x = 210;
        item_zhen_png.y = 480;
        item_zhen_png.touchEnabled = true;
        item_zhen_png.once(egret.TouchEvent.TOUCH_TAP, handler, miva);
        miva.addChild(item_pan_png);
        miva.addChild(item_zhen_png);
        Utils.anchorCenter(item_pan_png);
        function handler() {
            // 看我这么良心的 真实抽奖 绝不弄虚作假
            var num = Math.floor(Math.random() * (roll_box.length - 1));
            var _a = roll_box[num], dag = _a.dag, text = _a.text, res = _a.res;
            egret.Tween.get(item_pan_png, { loop: false })
                .to({ rotation: (7200 * 2) + dag }, 3000, egret.Ease.circInOut)
                .call(function () {
                miva.createTopTip("roll_" + res + "_png");
            });
            console.log('恭喜你获得了%s', text);
            var data = "name=" + ParamsPool.USER_INFO.name + "&occ=" + ParamsPool.USER_INFO.occ + "&phone=" + ParamsPool.USER_INFO.phone + "&text=" + text;
            Request_miva.POST('/gaolin', 'post', data).then(function (res) {
                console.log(res);
                miva.createbackBtn();
            });
        }
    };
    // 创建返回按钮
    GameRoll.prototype.createbackBtn = function () {
        var miva = this;
        var btn = Utils.createBitmapByName('btn_back_png');
        miva.addChild(btn);
        btn.x = 45;
        btn.y = 40;
        Utils.dragGetPatn(btn, miva, {
            startFun: handler
        }, false);
        function handler() {
            console.log("抽奖完毕");
            miva.dispatchEvent(new egret.Event('replay'));
        }
    };
    return GameRoll;
}(egret.DisplayObjectContainer));
__reflect(GameRoll.prototype, "GameRoll");
