var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
// 场景主控
var ScreenLayer = (function (_super) {
    __extends(ScreenLayer, _super);
    function ScreenLayer() {
        var _this = _super.call(this) || this;
        _this.startScreen = new StartScreen();
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    ScreenLayer.prototype.init = function () {
        var miva = this;
        // 初始化游戏配置
        miva.resetInit();
        // 设置本地数据库
        // IndexDB.SET_INDEXED_DB_FUN();
        var canFenxiang = false;
        setTimeout(function () {
            // 在这里执行一个查询事务
            var transaction = IndexDB.DB.transaction(['game_num']);
            var objectStore = transaction.objectStore('game_num');
            var req = objectStore.get(2);
            req.onsuccess = function (evt) {
                // 如果查成功了 处理结果
                if (req.result) {
                    console.log('查到了');
                    // 如果查到了并且时间不是今天, 那就让今天可以获得次数
                    if (req.result.day != new Date().getDate()) {
                        console.log('时间不是今天');
                        var req_1 = IndexDB.DB.transaction(['game_num'], 'readwrite')
                            .objectStore('game_num')
                            .put({ miva_id: 2, day: new Date().getDate(), is: true });
                        canFenxiang = true;
                        // 然后启动定时器, 判断是否分享了
                        pengyou();
                        return;
                    }
                    // 如果查到了, 但是今天可以获得次数就启动定时器, 判断是否分享了
                    if (req.result.is) {
                        console.log('可以分享');
                        canFenxiang = true;
                        pengyou();
                        return;
                    }
                    canFenxiang = false;
                }
                else {
                    // 如果没有查到就增加一条
                    console.log('没有查到, 增加了可分享的次数');
                    var req_2 = IndexDB.DB.transaction(['game_num'], 'readwrite')
                        .objectStore('game_num')
                        .add({ miva_id: 2, day: new Date().getDate(), is: true });
                    canFenxiang = true;
                    // 然后启动定时器, 判断是否分享了
                    pengyou();
                }
            };
        }, 2000);
        function pengyou() {
            setInterval(function () {
                try {
                    var miva_1 = document.querySelector("#miva");
                    if (miva_1.innerHTML == 'mimiva' && canFenxiang) {
                        ParamsPool.GAME_NUM++;
                        IndexDB.SET_GAME_NUM_FUN_IDB();
                        miva_1.innerHTML = '';
                        console.log('分享了, 消耗掉分享次数');
                        IndexDB.DB.transaction(['game_num'], 'readwrite')
                            .objectStore('game_num')
                            .put({ miva_id: 2, day: new Date().getDate(), is: false });
                        canFenxiang = false;
                    }
                }
                catch (err) { }
            }, 500);
        }
    };
    // 初始化
    ScreenLayer.prototype.resetInit = function () {
        var miva = this;
        var data = RES.getRes('ParamsPool_json');
        ParamsPool.QUESTION_BOX = RES.getRes("question_json");
        Object.assign(ParamsPool, data);
        // 题号
        ParamsPool.QUESTION_NUM = 1;
        // 答对数
        ParamsPool.QUESTION_SUCCESS = 0;
        ParamsPool.USER_INFO = {
            name: null,
            occ: null,
            phone: null,
        };
        // 设置本地数据库
        IndexDB.SET_INDEXED_DB_FUN();
        miva.addChild(miva.startScreen);
        miva.startScreen.addEventListener("start", miva.startGameHandler, this);
    };
    // 游戏开始处理器
    ScreenLayer.prototype.startGameHandler = function () {
        var miva = this;
        miva.removeChild(miva.startScreen);
        var getUserInfo = new GetUserInfo();
        // const getUserInfo = new GameRoll();
        miva.addChild(getUserInfo);
        getUserInfo.addEventListener("start", function () {
            miva.removeChild(getUserInfo);
            miva.runGameHandler();
        }, this);
    };
    // 游戏运行中处理器
    ScreenLayer.prototype.runGameHandler = function () {
        var miva = this;
        var gameStation = new GameStation();
        miva.addChild(gameStation);
        gameStation.addEventListener("next", function () {
            miva.removeChild(gameStation);
            miva.runGameHandler();
        }, this);
        gameStation.addEventListener("over", miva.gameOverHandler(gameStation), this);
    };
    // 游戏结束处理器
    ScreenLayer.prototype.gameOverHandler = function (gameStation) {
        var _this = this;
        return function () {
            var miva = _this;
            console.log("答题结束");
            miva.removeChild(gameStation);
            var gameEnd = new GameEnd();
            miva.addChild(gameEnd);
            gameEnd.addEventListener('replay', miva.resetHandler(gameEnd), miva);
            gameEnd.addEventListener('roll', miva.gotoGameRollhandler(gameEnd), miva);
        };
    };
    // 进入抽奖场景处理器
    ScreenLayer.prototype.gotoGameRollhandler = function (gameEnd) {
        var _this = this;
        return function () {
            var miva = _this;
            miva.removeChild(gameEnd);
            var gameRoll = new GameRoll();
            miva.addChild(gameRoll);
            gameRoll.addEventListener('replay', miva.resetHandler(gameRoll), miva);
        };
    };
    // 重置游戏处理器
    ScreenLayer.prototype.resetHandler = function (item) {
        var _this = this;
        return function () {
            var miva = _this;
            miva.removeChild(item);
            miva.resetInit();
        };
    };
    return ScreenLayer;
}(egret.DisplayObjectContainer));
__reflect(ScreenLayer.prototype, "ScreenLayer");
