var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
// 开始场景
var StartScreen = (function (_super) {
    __extends(StartScreen, _super);
    function StartScreen() {
        var _this = _super.call(this) || this;
        _this.start_bg_png = Utils.createBitmapByName('home_png');
        _this.isMusic = true;
        _this.once(egret.Event.ADDED_TO_STAGE, _this.init, _this);
        return _this;
    }
    StartScreen.prototype.init = function () {
        var miva = this;
        miva.addChild(miva.start_bg_png);
        miva.createStartBtn();
        miva.createRuleBtn();
        var text = Utils.createText("\u4ECA\u65E5\u5269\u4F59 " + ParamsPool.GAME_NUM + " \u6B21", { x: 215, y: 650 }, miva, false);
        miva.addEventListener(egret.Event.ENTER_FRAME, function () {
            text.text = "\u4ECA\u65E5\u5269\u4F59 " + ParamsPool.GAME_NUM + " \u6B21";
            text.size = 30;
        }, miva);
        miva.createAlert();
        ParamsPool.BGM = RES.getRes("mp3_mp3");
    };
    // 创建次数不足提示
    StartScreen.prototype.createNumTipAlert = function () {
        var miva = this;
        var tk_png = Utils.createBitmapByName("tk_png");
        miva.addChild(tk_png);
        Utils.anchorCenter(tk_png);
        tk_png.x = miva.stage.stageWidth / 2;
        tk_png.y = miva.stage.stageHeight / 2;
        egret.Tween.get(tk_png).wait(500).to({ alpha: 0 }, 200);
        console.log("游戏次数不足 !!");
    };
    // 创建开始按钮
    StartScreen.prototype.createStartBtn = function () {
        var miva = this;
        var start_btn = miva.start_btn = this.createBtn();
        start_btn.x = 185;
        start_btn.y = 845;
        this.addChild(start_btn);
        Utils.dragGetPatn(start_btn, this, {
            startFun: touchStartHandler
        }, false);
        function touchStartHandler() {
            if (ParamsPool.GAME_NUM <= 0) {
                miva.createNumTipAlert();
                return console.log("游戏次数不足!!");
            }
            console.log("游戏开始 !!");
            ParamsPool.GAME_NUM--;
            IndexDB.SET_GAME_NUM_FUN_IDB();
            miva.dispatchEvent(new egret.Event("start"));
        }
    };
    // 创建规则按钮
    StartScreen.prototype.createRuleBtn = function () {
        var miva = this;
        var rule_btn = this.createBtn();
        rule_btn.x = 185;
        rule_btn.y = 705;
        this.addChild(rule_btn);
        Utils.dragGetPatn(rule_btn, this, {
            startFun: touchStartHandler
        }, false);
        function touchStartHandler(evt) {
            console.log("\u67E5\u770B\u89C4\u5219");
            miva.createAlert();
        }
    };
    // 生成规则弹窗 可抽象为一个新工具
    StartScreen.prototype.createAlert = function () {
        var miva = this;
        var container = new egret.Sprite();
        miva.stage.addChild(container);
        Utils.createMask(container, handler);
        var alert_bg = Utils.createBitmapByName("rule_png");
        container.addChild(alert_bg);
        Utils.anchorCenter(alert_bg);
        alert_bg.x = miva.stage.stageWidth / 2;
        alert_bg.y = miva.stage.stageHeight / 2;
        alert_bg.touchEnabled = true;
        alert_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, Utils.superStopPropagation, miva);
        var close_btn = Utils.createBitmapByName("close_png");
        close_btn.x = 490;
        close_btn.y = 200;
        container.addChild(close_btn);
        close_btn.touchEnabled = true;
        close_btn.addEventListener(egret.TouchEvent.TOUCH_TAP, handler, miva);
        function handler(evt) {
            if (miva.isMusic) {
                ParamsPool.SOUND_CHANNEL = ParamsPool.BGM.play(0, -1);
                miva.isMusic = false;
            }
            miva.stage.removeChild(container);
        }
    };
    // 生成按钮方法
    StartScreen.prototype.createBtn = function () {
        var btn = new egret.Sprite();
        btn.graphics.beginFill(0, 0);
        btn.graphics.drawRect(0, 0, 260, 70);
        btn.graphics.endFill();
        btn.touchEnabled = true;
        return btn;
    };
    return StartScreen;
}(egret.DisplayObjectContainer));
__reflect(StartScreen.prototype, "StartScreen");
