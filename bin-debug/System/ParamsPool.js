var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
// 变量池
var ParamsPool = (function () {
    function ParamsPool() {
    }
    // 问题集
    ParamsPool.QUESTION_BOX = [];
    // 题号
    ParamsPool.QUESTION_NUM = 1;
    // 答对数
    ParamsPool.QUESTION_SUCCESS = 0;
    // 回答多少道题时结束
    ParamsPool.OVER_NUM = 10;
    // 正确多少道时可以抽奖
    ParamsPool.ROLL_NUM = 9;
    // 游戏次数
    ParamsPool.GAME_NUM = 1;
    // 游戏名
    ParamsPool.GAME_NAME = 'gaolin';
    // 游戏版本号
    ParamsPool.GAME_VERSION = '0.0.6';
    // 数据接口地址
    ParamsPool.BASE_RUL = 'http://59.110.214.170:23333';
    // 项目地址
    ParamsPool.PROJECT_RUL = '';
    // 用户信息
    ParamsPool.USER_INFO = {
        name: null,
        occ: null,
        phone: null,
    };
    // BGM
    ParamsPool.BGM = null;
    ParamsPool.SOUND_CHANNEL = null;
    return ParamsPool;
}());
__reflect(ParamsPool.prototype, "ParamsPool");
