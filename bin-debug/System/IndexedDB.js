var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
// 本地数据库
var IndexDB = (function () {
    function IndexDB() {
    }
    // 设置本地数据库
    IndexDB.SET_INDEXED_DB_FUN = function () {
        var indexedDB = window.indexedDB;
        var log = console.log;
        if (!indexedDB) {
            alert('你的设备设置不支持IndexedDB');
            window.location.href = ParamsPool.PROJECT_RUL + "?" + Math.random();
            return;
        }
        IndexDB.IDB = indexedDB.open(ParamsPool.GAME_NAME + "_" + ParamsPool.GAME_VERSION, 5);
        IndexDB.IDB.onsuccess = function (e) {
            log('本地数据库连接成功!');
            IndexDB.DB = e.target.result;
            log(IndexDB.DB);
            IndexDB.GET_GAME_NUM_FUN_IDB();
        };
        IndexDB.IDB.onerror = function (e) { return log(e.currentTarget.error.message); };
        IndexDB.IDB.onupgradeneeded = function (e) {
            log('数据库升级事件');
            IndexDB.DB = e.target.result;
            var objStore = null;
            if (!IndexDB.DB.objectStoreNames.contains('game_num')) {
                objStore = IndexDB.DB.createObjectStore('game_num', { keyPath: 'miva_id' });
            }
            log(objStore);
        };
    };
    // indexedDB初始化游戏次数方法
    IndexDB.ADD_GAME_NUM_FUN_IDB = function () {
        var log = console.log;
        var req = IndexDB.DB.transaction(['game_num'], 'readwrite')
            .objectStore('game_num')
            .add({ miva_id: 1, day: new Date().getDate(), num: ParamsPool.GAME_NUM });
        req.onsuccess = function (evt) {
            log('游戏次数设置成功, 剩余:', ParamsPool.GAME_NUM, '次');
        };
        req.onerror = function (evt) {
            log('游戏次数设置失败');
            alert('游戏次数设置失败(初始化)');
            window.location.href = ParamsPool.PROJECT_RUL + "?" + Math.random();
        };
    };
    // indexedDB设置游戏次数方法
    IndexDB.SET_GAME_NUM_FUN_IDB = function () {
        var log = console.log;
        var req = IndexDB.DB.transaction(['game_num'], 'readwrite')
            .objectStore('game_num')
            .put({ miva_id: 1, day: new Date().getDate(), num: ParamsPool.GAME_NUM });
        req.onsuccess = function (evt) {
            log('游戏次数设置成功, 剩余:', ParamsPool.GAME_NUM, '次');
        };
        req.onerror = function (evt) {
            log('游戏次数设置失败');
            alert('游戏次数设置失败(更新)');
            window.location.href = ParamsPool.PROJECT_RUL + "?" + Math.random();
        };
    };
    // 从indexedDb中读取游戏次数
    IndexDB.GET_GAME_NUM_FUN_IDB = function () {
        var log = console.log;
        var transaction = IndexDB.DB.transaction(['game_num']);
        var objectStore = transaction.objectStore('game_num');
        var req = objectStore.get(1);
        req.onerror = function (evt) {
            log('事务失败');
            window.location.href = ParamsPool.PROJECT_RUL + "?" + Math.random();
        };
        req.onsuccess = function (evt) {
            if (req.result) {
                log(req.result);
                if (req.result.day != new Date().getDate())
                    return IndexDB.SET_GAME_NUM_FUN_IDB();
                ParamsPool.GAME_NUM = req.result.num;
                // Start.txt.text = ` 今日剩余 ${ParamsPool.GAME_NUM} 次`;
                log(" \u4ECA\u65E5\u5269\u4F59 " + ParamsPool.GAME_NUM + " \u6B21");
            }
            else {
                log('未获得数据记录');
                IndexDB.ADD_GAME_NUM_FUN_IDB();
            }
        };
    };
    IndexDB.IDB = null;
    IndexDB.DB = null;
    return IndexDB;
}());
__reflect(IndexDB.prototype, "IndexDB");
