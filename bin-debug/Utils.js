var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
/**
 * author: KujoMiva
 * discription: egret utils package
 * 想整理又不想整理...  还是做项目的时候慢慢累积吧 = =
 */
var Utils = (function () {
    function Utils() {
    }
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    Utils.createBitmapByName = function (name) {
        var result = new egret.Bitmap();
        var texture = RES.getRes(name);
        result.texture = texture;
        return result;
    };
    /** 锚点居中, 传入一个显示对象, 设置它的锚点居中到自己正中心; */
    Utils.anchorCenter = function (item) {
        item.anchorOffsetX = item.width / 2;
        item.anchorOffsetY = item.height / 2;
    };
    /**取消事件冒泡 */
    Utils.superStopPropagation = function (evt) {
        evt.stopPropagation();
        evt.stopImmediatePropagation();
    };
    /** 视图对象拖动获取坐标方法 */
    Utils.dragGetPatn = function (target, container, funObj, isDrag) {
        if (isDrag === void 0) { isDrag = true; }
        /** 初始触摸点 */
        var startPoint = new egret.Point();
        var comput = new egret.Point();
        target.touchEnabled = true;
        target.addEventListener(egret.TouchEvent.TOUCH_BEGIN, touchBeginHandler, target);
        /** 开始触摸处理器 */
        function touchBeginHandler(evt) {
            startPoint.x = evt.stageX;
            startPoint.y = evt.stageY;
            target.addEventListener(egret.TouchEvent.TOUCH_MOVE, touchMoveHandler, target);
            target.addEventListener(egret.TouchEvent.TOUCH_END, touchEndHandler, target);
            container.setChildIndex(target, 99);
            /** 附加方法 */
            if (funObj && funObj.startFun)
                funObj.startFun(target, container);
        }
        /** 拖动处理器 */
        function touchMoveHandler(evt) {
            /** 附加方法 */
            if (funObj && funObj.moveFun)
                funObj.moveFun(target, container);
            /** 加入拖动开关, 默认开启拖动: 2019年3月11日 17:31:36 这里还有点小问题, 明天再改, 2019年3月12日 09:26:11 解决 */
            if (!isDrag)
                return;
            comput.x = evt.stageX - startPoint.x;
            comput.y = evt.stageY - startPoint.y;
            startPoint.x = evt.stageX;
            startPoint.y = evt.stageY;
            target.y += comput.y;
            target.x += comput.x;
        }
        /** 结束触摸处理器 */
        function touchEndHandler(evt) {
            console.log(target.x, target.y);
            target.removeEventListener(egret.TouchEvent.TOUCH_MOVE, touchMoveHandler, target);
            /** 附加方法 */
            if (funObj && funObj.endFun)
                funObj.endFun(target, container);
        }
    };
    ;
    /** 缩放添加视图对象 */
    Utils.createScale = function (target, container) {
        container.addChild(target);
        // target.scaleX = .5;
        // target.scaleY = .5;
    };
    /** 创建提示点 */
    Utils.createTipPoint = function () {
        var tipPoint = new egret.Sprite();
        tipPoint.graphics.beginFill(0xffffff, 1);
        tipPoint.graphics.drawCircle(0, 0, 10);
        tipPoint.graphics.endFill();
        tipPoint.graphics.beginFill(0xffffff, .3);
        tipPoint.graphics.drawCircle(0, 0, 20);
        tipPoint.graphics.endFill();
        tipPoint.graphics.lineStyle(3, 0xffffff);
        tipPoint.graphics.drawArc(0, 0, 20, 0, Math.PI / 180 * 60, true);
        tipPoint.graphics.endFill();
        egret.Tween.get(tipPoint, { loop: true })
            .to({ rotation: 180, scaleX: .5, scaleY: .5 }, 1000)
            .to({ rotation: 360, scaleX: 1, scaleY: 1 }, 1000);
        return tipPoint;
    };
    /** 道具提示动画 */
    Utils.itemAniamtion = function (target) {
        // Main.anchorCenter(target);
        egret.Tween.get(target, { loop: true })
            .to({ scaleX: target.scaleX * .8, scaleY: target.scaleY * .8 }, 500)
            .to({ scaleX: target.scaleX * 1.2, scaleY: target.scaleY * 1.2 }, 500)
            .to({ scaleX: target.scaleX, scaleY: target.scaleX }, 500)
            .wait(500)
            .to({ rotation: target.rotation + 10 }, 500)
            .to({ rotation: target.rotation + -10 }, 500)
            .to({ rotation: target.rotation + 10 }, 500)
            .to({ rotation: target.rotation + 0 }, 500)
            .wait(500);
    };
    /** 场景标题 */
    Utils.screenTitle = function (container, text) {
        var txt = new egret.TextField();
        container.addChild(txt);
        txt.text = text || "场景标题";
    };
    /** 创建遮罩 */
    Utils.createMask = function (container, diyFun) {
        var miva = container;
        var mask = new egret.Sprite();
        mask.graphics.beginFill(0, .5);
        mask.graphics.drawRect(0, 0, miva.stage.stageWidth, miva.stage.stageHeight);
        mask.graphics.endFill();
        mask.touchEnabled = true;
        mask.addEventListener(egret.TouchEvent.TOUCH_BEGIN, Utils.superStopPropagation, miva);
        mask.addEventListener(egret.TouchEvent.TOUCH_MOVE, Utils.superStopPropagation, miva);
        mask.addEventListener(egret.TouchEvent.TOUCH_END, Utils.superStopPropagation, miva);
        mask.addEventListener(egret.TouchEvent.TOUCH_CANCEL, Utils.superStopPropagation, miva);
        mask.addEventListener(egret.TouchEvent.TOUCH_TAP, handler, miva);
        miva.addChild(mask);
        function handler(evt) {
            Utils.superStopPropagation(evt);
            diyFun(evt);
        }
    };
    /**
     * 生成弹窗
     * 2019年6月6日 11:01:16 : 暂时先放置吧, 等项目完了回头来做
     */
    Utils.createAlert = function (target) {
        var miva = target;
        var container = new egret.Sprite();
        var alert_bg = Utils.createBitmapByName("rule_png");
        var close_btn = Utils.createBitmapByName("close_png");
        miva.stage.addChild(container);
        Utils.createMask(container, handler);
        function handler(evt) {
            miva.stage.removeChild(container);
        }
    };
    // 创建文字 
    Utils.createText = function (text, path, target, isDrag) {
        if (isDrag === void 0) { isDrag = true; }
        var miva = target;
        var textField = new egret.TextField();
        textField.text = text;
        textField.textColor = 0xf45050;
        textField.strokeColor = 0;
        textField.stroke = 3;
        textField.x = path.x;
        textField.y = path.y;
        textField.size = 55;
        textField.bold = true;
        miva.addChild(textField);
        if (isDrag)
            Utils.dragGetPatn(textField, miva);
        return textField; // 这里返回实例, 即可进行二次操作
    };
    return Utils;
}());
__reflect(Utils.prototype, "Utils");
